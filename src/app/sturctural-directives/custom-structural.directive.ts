import {Directive, Input, TemplateRef, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[appCustomStructural]'
})
export class CustomStructuralDirective {
  @Input()
  set appCustomStructural(product) {
    if (product.isAvailable === true) {
      this.viewContainerRef.createEmbeddedView(this.templateRef);
    }
  }

  constructor(private viewContainerRef: ViewContainerRef,
              private templateRef: TemplateRef<any>) { }

}
