import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-sturctural-directives',
  templateUrl: './sturctural-directives.component.html',
  styleUrls: ['./sturctural-directives.component.css']
})
export class StructuralDirectivesComponent implements OnInit {

  public title = "Structural Directives";
  public languages = [
    {name: 'PHP'},
    {name: 'JavaScript'},
    {name: 'Ruby'},
    {name: 'Java'},
    {name: 'Html5'}
  ];
  public show = true;
  public numberInString = "three";

  constructor() {
  }

  ngOnInit() {
  }

}
