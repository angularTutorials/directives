import {Directive, ElementRef, HostListener, Input} from '@angular/core';

@Directive({
  selector: '[appHighlightDirective]'
})
export class HighlightDirectiveDirective {

  @Input() highlightColor: string;
  @Input() hideOpacity: string;
  @Input() showOpacity: string;

  constructor(private elRef: ElementRef) {
    this.elRef.nativeElement.style.color = 'purple';
    this.elRef.nativeElement.style.opacity = 0.5;
  }

  @HostListener('mouseover') onMouseOver() {
    this.elRef.nativeElement.style.opacity = this.hideOpacity;
  }

  @HostListener('mouseout') onMouseOut() {
    this.elRef.nativeElement.style.opacity = this.showOpacity;
  }

  ngAfterViewInit(): void {
    this.elRef.nativeElement.style.color = this.highlightColor;
  }

}
