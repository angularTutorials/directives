import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule} from "@angular/forms";

import { AppComponent } from './app.component';
import { StructuralDirectivesComponent } from './sturctural-directives/sturctural-directives.component';
import { AttributeDirectivesComponent } from './attrubute-directives/attrubute-directives.component';
import { HighlightDirectiveDirective } from './attrubute-directives/highlight-directive.directive';
import { CustomStructuralDirective } from './sturctural-directives/custom-structural.directive';
import { ChangeDetectComponent } from './change-detect/change-detect.component';


@NgModule({
  declarations: [
    AppComponent,
    StructuralDirectivesComponent,
    AttributeDirectivesComponent,
    HighlightDirectiveDirective,
    CustomStructuralDirective,
    ChangeDetectComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
