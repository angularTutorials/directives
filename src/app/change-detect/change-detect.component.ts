import {Component, Input, OnInit} from '@angular/core';
import {User} from './user';

@Component({
  selector: 'app-change-detect',
  templateUrl: './change-detect.component.html',
  styleUrls: ['./change-detect.component.css']
})
export class ChangeDetectComponent implements OnInit {
  public title = 'Change detection';
  @Input()
  public user: User;
  constructor() { }

  ngOnInit() {
  }

}
