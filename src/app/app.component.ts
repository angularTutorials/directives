import {ChangeDetectionStrategy, Component} from '@angular/core';
import {User} from './change-detect/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent {
  user: User;
  public title = 'app';
  public products = [
    {productName: 'Shoes', isAvailable: true},
    {productName: 'Belts', isAvailable: true},
    {productName: 'Watches', isAvailable: false}
  ];
  constructor() {
    this.user = new User('defaultName', 0);
  }

  public changeDetectionDefault(): void {
    this.user.userName = 'Change Detection Default';
    this.user.userId = 10;
  }

  public changeDetectionOnPush(): void {
    this.user = new User('OnPush', 20);
  }
}
