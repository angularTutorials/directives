# Directives

## Angular Directive Types
1) Component Directive - usual Angular component;
2) Structural Directive - directives that alter or transform the DOM elements (one or more) on the fly. These directives is marked with symbol *, such as ngFor, ngIf, ngSwitch and etc...;
3) Attribute Directive - directives that extend the behavior or look and feel of an element

### Component Directive
Nothing new, its a simple Angular @Component, which has controller, template, selection and etc.

### Structural Directive

Please see below some of structural directive examples:

*ngFor:
```
<ul>
  <li *ngFor = let language of languages>{{language.name}}</li>
</ul>
```

*ngIf:
```
<span *ngIf=true>
  <p>Show Me if true</p>
</span>
```

*ngSwitch:
```
<span [ngSwitch]=taxRate>
<p *ngSwitchCase='state'> State Tax</p>
<p *ngSwitchCase='fedral'> Fedral Tax</p>
<p *ngSwitchCase='medical'> Medical Tax</p>
</span>
```

### Attribute Directive

Attribute directives are built-in or custom. Some angular built-in directives:

ngModel - for tow way data binding;
```
<input [(ngModel)] = "name">
<p>the name is: {{name}}</p>
```

ngClass - add class for DOM element;

The class names could be assigned using strings:
```
<div [ngClass] = "'className'">Example</div>
```

The class names could be assigned using arrays:
```
<div [ngClass] = "['classNameOne', 'classNameTwo']">Example</div>
```

The class names could be assigned passing objects:
```
<div [ngClass] = "{'error': true, 'success':false}">Example</div>
```

The class names could be assigned passing component method:
```
<div [ngClass] = "getClassName('className')">Example</div>
```

ngStyle - add styles for DOM element

### Custom Directive

Create custom directive with Angular CLI:

```ng generate directive directive_name```

Directive could have @Input 


